import React from 'react';
import { IPost } from '..';
import Link from 'next/link';


const Post = ({post}:{post:IPost}) => {
    return (
        <main>
            <Link href={"/"}>
                <a> Revenir </a>
            </Link>
            
            <h1> {post.title} </h1>
            <p> {post.body} </p>
        </main>
    )
}

export default Post;

export  const getServerSideProps = async({params}:{params:any}) => {
    const post:IPost = await fetch(`http://jsonplaceholder.typicode.com/posts/${params.id}`).then(r => r.json())
    return {
        props:{
            post
        }
    }
}

/*
export const getStaticPaths = async() => {
    const posts:IPost[] = await fetch('http://jsonplaceholder.typicode.com/posts?_limit=4').then(r => r.json())
        return {
            paths: posts.map(post =>({
                params: {id: post.id.toString()}
            })),
            fallback: false,
        }
}
*/